require 'net/http'
require 'Qt4'

class AppCliente < Qt::Widget
slots 'app()'

  def initialize
      super
      setWindowTitle "HyperZen Client"
      init_ui
      resize 500, 500
      show
  end

  def init_ui
    letra=14
    gridLayout = Qt::GridLayout.new(self)
    text="Escriba su clave secreta:"
    label = Qt::Label.new text, self
    label.setFont(Qt::Font.new('Times', letra, Qt::Font::Bold))
    @button = Qt::PushButton.new('Enviar')
    @button.setFont(Qt::Font.new('Times', letra, Qt::Font::Bold))
    @textField = Qt::LineEdit.new self
    @textField.setFont(Qt::Font.new('Times', 12, Qt::Font::Bold))
    @areaField = Qt::TextEdit.new "==================================================
    \n===============Registro de acontecimientos==============
    \n==================================================\n", self
    @areaField.setFont(Qt::Font.new('Times', 12, Qt::Font::Bold))
    connect @button, SIGNAL("clicked()"),
            self, SLOT("app()")

    gridLayout.addWidget(label, 0, 0)
    gridLayout.addWidget(@textField, 0, 1)
    gridLayout.addWidget(@button, 0, 3)
    gridLayout.addWidget(@areaField, 1, 0,1,4)
   end


def app

  @BaseURL='http://hyperzen.tk/'
  @clave=@textField.text.gsub(" ","")

  comprobarClave=Net::HTTP.get(URI(@BaseURL+'key/'+@clave)).split('').reverse.drop(1).reverse.join

  if comprobarClave.eql? "true"
    @button.setVisible false
@areaField.append 'Su clave es Correcta.'
@areaField.append 'Conectando con el sistema...'
  updateInfo
  else
@areaField.append 'Su clave es Incorrecta, por favor revise su clave.'
  end
end

def comandosHyperZen
  uri = URI(@BaseURL+'ordenes/'+@clave+'/'+@maquina)
  comandos=Net::HTTP.get(uri)
  comandos.split('<br />').reverse.drop(1).reverse.each {|orden|

    idComando=orden.split('&nbsp;')[0].to_i
    cmd=orden.split('&nbsp;').drop(1).join.gsub("&amp;","&").gsub("&quot;","\"").gsub("&#39;","\'").gsub("&gt;",">").gsub("`","")
    comando=system(cmd)
    puts 'Ejecutando '+cmd
    begin
    @areaField.append 'Ejecutando '+cmd
  rescue
  end
    if comando
      begin
  @areaField.append 'Ejecutado con exito.'
rescue
end
    Net::HTTP.get(URI(@BaseURL+'delOrdenClient/'+@clave+'/'+@maquina+'/'+idComando.to_s))
    initInfo
    else
      begin
  @areaField.append 'Ejecucion fallida.'
rescue
end
    end

  }
  sleep(5)
comandosHyperZen
end

def initInfo
  getlistmvs
  uri = URI(@BaseURL+'comandos')
  comandos=Net::HTTP.get(uri)
  comandos.split('<br />').reverse.drop(1).reverse.each {|comando|
    if @maquina.split("/")[0].casecmp(comando.split("##")[2])==0
    initvms(comando.split("##")[1],comando.split("##")[3].gsub("&amp;","&").gsub("&quot;","\"").gsub("&#39;","\'").gsub("&gt;",">").gsub("`",""))
    end
  }
  delmvs
end

def getlistmvs
  @listaMVS=Array.new
  uri = URI(@BaseURL+'getlistmvs/'+@clave+'/'+@maquina)
  mvs=Net::HTTP.get(uri)
  mvs.split('<br />').reverse.drop(1).reverse.each {|mv|
    @listaMVS.push([mv.split("##")[0],mv.split("##")[1],mv.split("##")[2].gsub("&amp;","&").gsub("&quot;","\"").gsub("&#39;","\'").gsub("&gt;",">").gsub("`",""),0])
  }
end

def delmvs
  @listaMVS.each { |mv|
      if mv[3].to_i==0
        uri = URI(@BaseURL+'delMaquinaVirtual/'+@clave+'/'+@maquina+'/'+mv[0].to_i.to_s)
        Net::HTTP.get(uri)
      end
   }
end

def initvms(hyperv,comando)
  hypervisor=hyperv
  mvs=`#{comando}`
  mvs.split("\"---mv---\"").drop(1).each {|mv|
    begin
      puts mv
  nombre=mv.split("\n")[1]
  ruta=mv.split("\n")[2]
  memoriaTotal=mv.split("\n")[3].to_i/1024/1024
  memoria=toMB(mv.split("\n")[5])
  estado=mv.split("\n")[6]
  rutaHDD=mv.split("\n")[7]
  sizeTotalMV=toMB(mv.split("\n")[8])
  sizeOcupadoMV=toMB(mv.split("\n")[9])
  sizeTotalMaquina=toMB(mv.split("\n")[10])
    snapshots=""
    mv.split("galeria de snapshots")[1].split("\n").drop(1).each { |snap|
      if isNumber(snap.gsub(" ","")) or snap.gsub(" ","").empty?
		break
	  else
	  snapshots+=snap+";"
      end
    }
    if(format(snapshots)!="")
    url=format(hypervisor)+'/'+format(nombre)+'/'+format(ruta)+'/'+format(memoriaTotal)+'/'+format(memoria)+'/'+format(estado)+'/'+format(rutaHDD)+'/'+format(sizeTotalMV)+'/'+format(sizeOcupadoMV)+'/'+format(sizeTotalMaquina)+'/'+format(snapshots)
    else
      url=format(hypervisor)+'/'+format(nombre)+'/'+format(ruta)+'/'+format(memoriaTotal)+'/'+format(memoria)+'/'+format(estado)+'/'+format(rutaHDD)+'/'+format(sizeTotalMV)+'/'+format(sizeOcupadoMV)+'/'+format(sizeTotalMaquina)+'/'+"null"
    end

    begin
    uri =URI(URI.escape(@BaseURL+'mv/'+@clave+'/'+@maquina+'/'+url))
    Net::HTTP.get(uri)
    rescue
      puts "error al enviar peticion"
    end

    @listaMVS.each { |mv|
        if mv[1].eql? hypervisor
          if mv[2].eql? nombre
              mv[3]=1
          end
        end
     }

begin
  @areaField.append hypervisor
  @areaField.append nombre
  @areaField.append ruta
  @areaField.append memoriaTotal.to_s
  @areaField.append memoria.to_s
  @areaField.append estado
  @areaField.append rutaHDD
  @areaField.append sizeTotalMV.to_s
  @areaField.append sizeOcupadoMV.to_s
  @areaField.append sizeTotalMaquina.to_s
  @areaField.append snapshots
rescue
end
rescue
  puts "Algo a ido mal"
end
}
end

def format(e)
  return URI.escape(e.to_s.gsub(".","_{}_").gsub(":","%3A").gsub(";","%3B").gsub("/","%2F"))
end

def toMB(e)
  valor=e.split(" ")[0].to_i
  ponderacion=e.split(" ")[1]

  if ponderacion=="Bytes"
    return valor/1024/1024
  elsif ponderacion=="KB"
    return valor/1024
  elsif ponderacion=="GB"
    return valor*1024
  elsif ponderacion=="TB"
    return valor*1024*1024
  else
    return valor
  end

end

def isNumber string
  true if Float(string) rescue false
end

def updateInfo
  recuperarOS
  uri = URI(@BaseURL+'existeMaquina/'+@clave+'/'+@maquina)
  existeMaquina=Net::HTTP.get(uri).split('').reverse.drop(1).reverse.join

  if existeMaquina.eql? "true"
    @areaField.append "Conexion establecida."
    @areaField.append "Esperando acciones..."
    @areaField.append "PD: Las acciones se ejecutarán en segundo plano."
    Thread.new{initInfo}
      Thread.new{comandosHyperZen}
  else
    @areaField.append "Creando Maquina..."
    uri = URI(@BaseURL+'crearMaquina/'+@clave+'/'+@maquina)
    Net::HTTP.get(uri)
	updateInfo
  end
end

def recuperarOS
  hostname=`hostname`
    @maquina="deb-based"+"/#{hostname}".split('').reverse.drop(1).reverse.join

  @areaField.append @maquina

end
end

app = Qt::Application.new ARGV
AppCliente.new
app.exec
