class CreateSnapshots < ActiveRecord::Migration
  def change
    create_table :snapshots do |t|
      t.string :nombre
      t.string :direccionArchivo

      t.timestamps null: false
    end
  end
end
