class Maquina < ActiveRecord::Base
  belongs_to :usuario
  has_many :ordens, :dependent => :destroy
  has_many :maquina_virtuals, :dependent => :destroy
  has_many :comandos, :dependent => :destroy
  
  validates :nombre ,:SO , presence: true
  
  validates :SO, length: { in: 3..255 , message: "debe tener entre 3 y 255 caracteres"}
end
