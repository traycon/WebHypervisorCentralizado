require 'test_helper'
include SessionsHelper
class OrdensControllerTest < ActionController::TestCase
  setup do
    @orden = ordens(:one)
      @user = Usuario.new
       @user.email=  "prueba@prueba.es"
       @user.password= "123456aa"
       @user.username= "prueba"
        @user.auth="1234"
       @user.save
       log_in @user
    @maquina=Maquina.new
    @maquina.nombre="prueba-pc"
    @maquina.SO="Windows"
    @maquina.usuario_id=@user.id
    @maquina.save
    @orden.maquina_id=@maquina.id
    @orden.save
  end
  
  def teardown
    @user.destroy
    log_out
  end

  test "ordenes" do
    get :ordenes, key: @user.auth, so: @maquina.SO ,host: @maquina.nombre
    assert_response :success
  end
  
    test "existeUser" do
    get :existe_user, id: @user.auth
    assert_response :success
  end
  
    test "existeMaquina" do
    get :existeMaquina, key: @user.auth, so: @maquina.SO ,host: @maquina.nombre
    assert_response :success
  end
  
      test "delete" do
    get :delete, key: @user.auth, so: @maquina.SO ,host: @maquina.nombre, id: @orden.id
    assert_redirected_to root_path
  end
  
    test "should destroy orden" do
    log_out
      get :destroy, id: @orden.id
    assert_redirected_to root_path
    
    log_in @user
    assert_difference('Orden.count', -1) do
      get :destroy, id: @orden.id
    end

    assert_redirected_to root_path
  end
  
end
