class MaquinaVirtual < ActiveRecord::Base
  belongs_to :maquina
  has_many :snapshots, :dependent => :destroy
  
  validates :nombre ,:estado,:SO,:opciones , :hypervisor , presence: true
  
  validates :nombre, length: { in: 3..255 , message: "debe tener entre 3 y 255 caracteres"}
  validates :hypervisor, length: { in: 3..32 , message: "debe tener entre 3 y 32 caracteres"}
  validates :SO, length: { in: 3..255 , message: "debe tener entre 3 y 255 caracteres"}
  validates :estado, length: { in: 3..32 , message: "debe tener entre 3 y 32 caracteres"}

end
