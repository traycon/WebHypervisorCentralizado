require 'test_helper'
include SessionsHelper
class MaquinasControllerTest < ActionController::TestCase
  setup do
    @maquina = maquinas(:one)
      @user = Usuario.new
       @user.email=  "prueba@prueba.es"
       @user.password= "123456aa"
       @user.username= "prueba"
       @user.auth="1234"
       @user.save
       log_in @user
  end
  
  def teardown
    @user.destroy
    log_out
  end

  test "should show maquina" do
    get :show, id: @maquina
    assert_response :success
    
    log_out
    
    get :show, id: @maquina
    assert_redirected_to root_path
  end

  test "should destroy maquina" do
    assert_difference('Maquina.count', -1) do
      delete :destroy, id: @maquina
    end

    assert_redirected_to root_path
  end
  
    test "should create maquina" do
    get :crearMaquina,key: @user.auth, so: "Windows" ,host: "prueba"
    assert_redirected_to root_path
  end
  
end
