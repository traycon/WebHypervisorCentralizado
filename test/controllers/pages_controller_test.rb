require 'test_helper'
include SessionsHelper
class PagesControllerTest < ActionController::TestCase
  
     setup do
       @user = Usuario.new
       @user.email=  "prueba@prueba.es"
       @user.password= "123456aa"
       @user.username= "prueba"
       @user.save
     end
     
      def teardown
    @user.destroy
    log_out
  end
  
    test "should get accesoLogin" do
    post :accesologin, usuario: { emailLogin: "", passwordLogin: "" }
    assert_response :success
    
    post :accesologin, usuario: { emailLogin: "prueba@prueba.es", passwordLogin: "" }
    assert_response :success
    
    post :accesologin, usuario: { emailLogin: "prueba@prueba.es", passwordLogin: "123456aa" }
    assert_redirected_to root_path
  end
  
    test "should get home" do
    get :home
    assert_response :success
  end
  
      test "login" do
    get :login
    assert_response :success
    log_in @user
    get :login
    assert_redirected_to root_path
  end
  
    test "should get salir" do
    get :salir
    
    assert_redirected_to root_path
  end
  
end
