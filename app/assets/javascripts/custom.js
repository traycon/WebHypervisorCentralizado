$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});



//Validacion login
function notNull(elemento) {
 	var Nombre=elemento.value;
	
   if(!Nombre || /^\s*$/.test(Nombre) ||Nombre=="" || Nombre=="Seleccione un tipo" || Nombre=="Seleccione una categoria" || Nombre=="Seleccione un impacto"){
   	elemento.style.borderColor='red';
   	return false;
   }else{
		elemento.style.borderColor='green';
		return true;
	}
  
}

function validarEmail(mail){
  	var email=mail.value;
     expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!email || /^\s*$/.test(email) ||email==""){
    	mail.style.borderColor='red';
    	return false;
    }else if ( !expr.test(email) ){
	mail.style.borderColor='red';
	return false;
	}else{
		mail.style.borderColor='green';
		return true;
}
  	
}


var contrasena;
var recontrasena;
function validarContrasena(pass){
	var digitos=0;
	var otros=0;
	contrasena=pass.value;
	for ( var i=0; i<contrasena.length;i++) {
		if(contrasena.charAt(i)>='0' && contrasena.charAt(i)<='9'){
			digitos++;
		}else{
			otros++;
		}
	}
	if(digitos>=2&&otros>=2&&digitos+otros>=8&&digitos+otros<=255){
		pass.style.borderColor='green';
		return true;
	}else{
		pass.style.borderColor='red';
		return false;
	}
	
}

function igualPass(pass,pass2){
	if(pass.length==pass2.length){
	for ( var i=0; i<pass.length;i++) {
		if(pass.charAt(i)!=pass2.charAt(i)){
			return false;
		}
	}
	return true;
	}else{
		return false;
	}
}

function validacionRegistro() {
	var nombre=document.getElementById("usuario_username");
	var email=document.getElementById("usuario_email");
	var contrasena=document.getElementById("usuario_password");
	var contrasena2=document.getElementById("usuario_password2");
	if(notNull(nombre)&&validarEmail(email)&&validarContrasena(contrasena)&&validarContrasena(contrasena2)&&igualPass(contrasena,contrasena2)){
		return true;
	}else{
		return false;
	}
	
}

function validarPass() {
	var contrasena=document.getElementById("usuario_password");
	var contrasena2=document.getElementById("usuario_password2");
	if(validarContrasena(contrasena)&&validarContrasena(contrasena2)&&igualPass(contrasena,contrasena2)){
		return true;
	}else{
		return false;
	}
	
}

