class UsuariosController < ApplicationController
    before_action :require_login
 skip_before_action :require_login, only: [:new, :create]
  before_action :set_usuario, only: [:show, :update, :destroy, :newKey]

 
   def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    current_user
  end

  # GET /usuarios/new
  def new
     current_user
    @usuario = Usuario.new
    
    if @current_user!=nil
      redirect_to root_path
    end
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    current_user
    @usuario = Usuario.new(usuario_params)
    @usuario.resetPassword=0
    randomAuth=Array.new(32){[*"A".."Z", *"0".."9"].sample}.join
    while Usuario.find_by(auth: randomAuth)!=nil do
    randomAuth=Array.new(32){[*"A".."Z", *"0".."9"].sample}.join
    end
    @usuario.auth=randomAuth
    respond_to do |format|
      if @usuario.save
        log_in @usuario
        format.html { redirect_to root_path, notice: '' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def newKey
    current_user
    randomAuth=Array.new(32){[*"A".."Z", *"0".."9"].sample}.join
    while Usuario.find_by(auth: randomAuth)!=nil do
    randomAuth=Array.new(32){[*"A".."Z", *"0".."9"].sample}.join
    end
   
    respond_to do |format|
      if @usuario.update(:auth => randomAuth)
        format.html { redirect_to @usuario, notice: 'Actualizado con éxito.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      current_user
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Actualizado con éxito.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    current_user
    @usuario.destroy
    log_out
    respond_to do |format|
      format.html { redirect_to root_path, notice: '' }
      format.json { head :no_content }
    end
  end
 
  
  private
    # Use callbacks to share common setup or constraints between actions.
def set_usuario
      current_user
      if !numeric(params[:id])
        redirect_to usuarios_path(:id => @current_user["id"])
      else
        if @current_user["id"]!=Float(params[:id])
          redirect_to usuarios_path(:id => @current_user["id"])
        else
      @usuario = Usuario.find(params[:id])
      end
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:username, :password, :email, :diferenciaHoraria, :auth, :resetPassword)
    end
end
