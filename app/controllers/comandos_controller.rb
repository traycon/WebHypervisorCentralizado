class ComandosController < ApplicationController
  
  before_action :set_comando, only: [:show, :edit, :update, :destroy]
  layout false, only: [:index, :getlistmvs]
 before_action :require_login
 skip_before_action :require_login, only: [:index, :getlistmvs]

   def require_login
     current_user
    unless @current_user!=nil and (@current_user["diferenciaHoraria"]==1548 or @current_user["email"]=="centerbot0001@gmail.com")
      redirect_to root_path
    end
  end
 
  # GET /comandos
  # GET /comandos.json
  def index
    @comandos = Comando.all
  end
  
  def listaComandos
    current_user
    @comandos = Comando.all
  end
  
  def getlistmvs
    user = Usuario.find_by(auth: params[:key])
    if user!=nil
     maquina=user.maquinas.find_by(SO: params[:so],nombre: params[:host])
      if maquina!=nil
        @maquinavirtual=user.maquinas.find_by(SO: params[:so],nombre: params[:host]).maquina_virtuals
      end
    end
  end
  # GET /comandos/1
  # GET /comandos/1.json
  def show
    current_user
  end

  # GET /comandos/new
  def new
    current_user
    @comando = Comando.new
  end

  # GET /comandos/1/edit
  def edit
    current_user
  end

  # POST /comandos
  # POST /comandos.json
  def create
    current_user
    @comando = Comando.new(comando_params)

    respond_to do |format|
      if @comando.save
        format.html { redirect_to listaComandos_path, notice: '' }
        format.json { render :show, status: :created, location: @comando }
      else
        format.html { render :new }
        format.json { render json: @comando.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comandos/1
  # PATCH/PUT /comandos/1.json
  def update
    current_user
    respond_to do |format|
      if @comando.update(comando_params)
        format.html { redirect_to listaComandos_path, notice: '' }
        format.json { render :show, status: :ok, location: @comando }
      else
        format.html { render :edit }
        format.json { render json: @comando.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comandos/1
  # DELETE /comandos/1.json
  def destroy
    current_user
    @comando.destroy
    respond_to do |format|
      format.html { redirect_to listaComandos_path, notice: '' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comando
      @comando = Comando.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comando_params
      params.require(:comando).permit(:nombre, :comandoParam, :hypervisor, :version)
    end
end
