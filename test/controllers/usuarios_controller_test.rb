require 'test_helper'

class UsuariosControllerTest < ActionController::TestCase
  setup do
  @user = Usuario.new
       @user.email=  "prueba@prueba.es"
       @user.password= "123456aa"
       @user.username= "prueba"
       @user.auth="1234"
       @user.save
       log_in @user
  end
  
  def teardown
    @user.destroy
    log_out
  end

  test "should get new" do
    get :new
    assert_redirected_to root_path
    
    log_out
    get :new
    assert_response :success
  end
  
    test "Crear Usuario" do
      log_out
     assert_difference('Usuario.count') do
      post :create, usuario: { username: "patatas", password: "bacalao12", email: "asd@asd.asd" }
    end

    assert_redirected_to root_path
  end

  test "should get show" do
    get :show, id: @user
    assert_response :success
    
    log_out
    get :show, id: @user
    assert_redirected_to root_path
  end

  test "should get newKey" do
    get :newKey, id: @user
    assert_redirected_to @user
  end
  
  test "should get update" do
    put :update, id: @user, usuario: {username: "pepito"}
    assert_redirected_to @user
  end
  
    test "should destroy usuario" do
    assert_difference('Usuario.count', -1) do
      get :destroy, id: @user
    end

    assert_redirected_to root_path
  end
  
end
