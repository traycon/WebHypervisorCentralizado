class MaquinasController < ApplicationController
  before_action :set_maquina, only: [:show,:destroy]
  before_action :require_login
 skip_before_action :require_login, only: [:crearMaquina]
 
   def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  
  def show
    current_user
  end
  
  def crearMaquina
    current_user
    user = Usuario.find_by(auth: params[:key])
   if user!=nil
     maquina=user.maquinas.find_by(SO: params[:so],nombre: params[:host])
     
     if maquina==nil
       maquina = user.maquinas.build
       maquina.SO=params[:so]
       maquina.nombre=params[:host]
       maquina.save
       
     end
   end
   redirect_to root_path
  end

  # DELETE /maquinas/1
  # DELETE /maquinas/1.json
  def destroy
    current_user
    @maquina.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: '' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_maquina
      @maquina = Maquina.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maquina_params
      params.require(:maquina).permit(:nombre, :SO)
    end
end
