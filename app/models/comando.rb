class Comando < ActiveRecord::Base
  belongs_to :maquina
  
  validates :nombre ,:comandoParam , :hypervisor , presence: true
  
  validates :nombre, length: { in: 3..255 , message: "debe tener entre 3 y 255 caracteres"}
  validates :hypervisor, length: { in: 3..32 , message: "debe tener entre 3 y 32 caracteres"}
end
