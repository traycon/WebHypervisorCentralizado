class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :username
      t.string :password
      t.string :email
      t.integer :diferenciaHoraria
      t.string :auth
      t.integer :resetPassword

      t.timestamps null: false
    end
  end
end
