module ApplicationHelper
  
def user(user)
  content_for :user, user
  if user!=nil
  content_for :username, user["username"]
  content_for :iduser, user["id"].to_s.gsub('%','')
  end
end

end
