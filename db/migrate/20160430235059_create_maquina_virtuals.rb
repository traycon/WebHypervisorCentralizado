class CreateMaquinaVirtuals < ActiveRecord::Migration
  def change
    create_table :maquina_virtuals do |t|
      t.string :nombre
      t.string :estado
      t.string :SO
      t.string :opciones
      t.string :hypervisor
      t.string :versionHypervisor

      t.timestamps null: false
    end
  end
end
