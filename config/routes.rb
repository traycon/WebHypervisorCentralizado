Rails.application.routes.draw do
  resources :comandos
 
  root 'pages#home'
  get 'home' => 'pages#home'
  get 'login' => 'pages#login'
  post 'login' => 'pages#accesologin'
  get 'registrate' => 'usuarios#new'
  post 'usuarios' => 'usuarios#create'
  patch 'usuarios/:id' => 'usuarios#update'
  put 'usuarios/:id' => 'usuarios#update'
  get 'salir' => 'pages#salir'
  get 'delComando/:id'=> 'comandos#destroy', as: :delComando
  get 'delSnapshot/:id'=> 'snapshots#destroy', as: :delSnapshot
  get 'delOrden/:id'=> 'ordens#destroy', as: :delOrden
  get 'maquinaVirtual/:id'=> 'maquina_virtuals#show', as: :maquina_virtual
  get 'delMaquinaVirtual/:key/:so/:host/:id'=> 'maquina_virtuals#destroy', as: :delMaquinaVirtual
  get 'maquinas/:id'=> 'maquinas#show', as: :maquina
  get 'delMaquina/:id'=> 'maquinas#destroy', as: :delMaquina
  get 'delUsuario/:id'=> 'usuarios#destroy', as: :delUsuario
  get 'perfil/:id'=> 'usuarios#show', as: :perfil
  get 'usuarios/:id' => 'usuarios#show', as: :usuario
  get 'newKey/:id'=> 'usuarios#newKey', as: :newKey
  get 'key/:id' => 'ordens#existe_user'
  get 'so' => 'ordens#SO'
  get 'existeMaquina/:key/:so/:host' => 'ordens#existeMaquina'
  get 'crearMaquina/:key/:so/:host' => 'maquinas#crearMaquina'
  get 'ordenes/:key/:so/:host' => 'ordens#ordenes'
  get 'delOrdenClient/:key/:so/:host/:id'=> 'ordens#delete'
  get 'actualizarMV/:id'=> 'maquina_virtuals#actualizarMV', as: :actualizarMV
  get 'vnc/:id' => 'maquina_virtuals#vnc', as: :vnc
  get 'mv/:key/:so/:host/:hypervisor/:nombre/:ruta/:memoriaTotal/:memoria/:estado/:rutaHDD/:sizeTotalMV/:sizeOcupadoMV/:sizeTotalMaquina/:snapshots' => 'maquina_virtuals#mv', as: :mv
  get 'getlistmvs/:key/:so/:host' => 'comandos#getlistmvs'
  get 'accion/:comando/:id/:valor' => 'maquina_virtuals#accion', as: :accion
  get 'changevnc/:id/:ipvnc/:portvnc'=> 'maquina_virtuals#changevnc'
  get 'listaComandos'=> 'comandos#listaComandos'
  get '*path' => redirect('/')
end
