require 'test_helper'
include SessionsHelper
class ComandosControllerTest < ActionController::TestCase
  setup do
    @comando = comandos(:one)
    @user = Usuario.new
       @user.email=  "prueba@prueba.es"
       @user.password= "123456aa"
       @user.username= "prueba"
       @user.diferenciaHoraria="1548"
       @user.auth="1234"
       @user.save
       log_in @user
       
    @maquina=Maquina.new
    @maquina.nombre="prueba-pc"
    @maquina.SO="Windows"
    @maquina.usuario_id=@user.id
    @maquina.save
  end
  
  def teardown
    @user.destroy
    log_out
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:comandos)
  end
  
  test "should get listaComandos" do
    get :listaComandos
    assert_response :success
    assert_not_nil assigns(:comandos)
  end

  test "should get new" do
    get :new
    assert_response :success
    
    @user.diferenciaHoraria="1"
    @user.save
    
    get :new
    assert_redirected_to root_path
  end

  test "should create comando" do
    assert_difference('Comando.count') do
      post :create, comando: { comandoParam: @comando.comandoParam, hypervisor: @comando.hypervisor, nombre: @comando.nombre, version: @comando.version }
    end

    assert_redirected_to listaComandos_path
  end

  test "should show comando" do
    get :show, id: @comando
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comando
    assert_response :success
  end

  test "should update comando" do
    patch :update, id: @comando, comando: { comandoParam: @comando.comandoParam, hypervisor: @comando.hypervisor, nombre: @comando.nombre, version: @comando.version }
    assert_redirected_to listaComandos_path
  end

  test "should destroy comando" do
    assert_difference('Comando.count', -1) do
      delete :destroy, id: @comando
    end

    assert_redirected_to listaComandos_path
  end
  
    test "should show getlistmvs" do
    get :getlistmvs, key: @user.auth, so: @maquina.SO ,host: @maquina.nombre
    assert_response :success
  end
  
end
