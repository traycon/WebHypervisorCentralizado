class Usuario < ActiveRecord::Base
  has_many :maquinas, :dependent => :destroy
  
  validates :email ,:username , :password , presence: true 
  # Valida en una expresion regular el email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { :with => VALID_EMAIL_REGEX , message: "El formato del correo es invalido" }

  validates :username, length: { in: 3..255 , message: "debe tener entre 3 y 255 caracteres"}
  validates :username, uniqueness: {case_sensitive: false ,message: "ya esta registrado"}
  validates :password, length: { in: 8..255 , message: "debe tener entre 8 y 255 caracteres"}
  # Validamos que el email sea unico
  validates :email, uniqueness: {case_sensitive: false ,message: "ya esta registrado"}
end
