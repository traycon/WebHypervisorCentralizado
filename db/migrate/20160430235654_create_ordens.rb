class CreateOrdens < ActiveRecord::Migration
  def change
    create_table :ordens do |t|
      t.string :comando

      t.timestamps null: false
    end
  end
end
