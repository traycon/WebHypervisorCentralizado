# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( jquery-1.11.1.min.js )
Rails.application.config.assets.precompile += %w( modernizr.custom.js )
Rails.application.config.assets.precompile += %w( wow.min.js )
Rails.application.config.assets.precompile += %w( metisMenu.min.js )
Rails.application.config.assets.precompile += %w( custom.js )
Rails.application.config.assets.precompile += %w( bootstrap.js )
Rails.application.config.assets.precompile += %w( Chart.js )
Rails.application.config.assets.precompile += %w( classie.js )
Rails.application.config.assets.precompile += %w( clndr.js )
Rails.application.config.assets.precompile += %w( jquery.circlechart.js )
Rails.application.config.assets.precompile += %w( jquery.nicescroll.js )
Rails.application.config.assets.precompile += %w( jquery.vmap.js )
Rails.application.config.assets.precompile += %w( jquery.vmap.sampledata.js )
Rails.application.config.assets.precompile += %w( jquery.vmap.world.js )
Rails.application.config.assets.precompile += %w( moment-2.2.1.js )
Rails.application.config.assets.precompile += %w( scripts.js )
Rails.application.config.assets.precompile += %w( site.js )
Rails.application.config.assets.precompile += %w( skycons.js )
Rails.application.config.assets.precompile += %w( underscore-min.js )
Rails.application.config.assets.precompile += %w( validator.min.js )
