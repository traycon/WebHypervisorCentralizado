# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160818230754) do

  create_table "comandos", force: :cascade do |t|
    t.string   "nombre"
    t.string   "comandoParam"
    t.string   "hypervisor"
    t.string   "version"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "maquina_id"
  end

  create_table "maquina_virtuals", force: :cascade do |t|
    t.string   "nombre"
    t.string   "estado"
    t.string   "SO"
    t.string   "opciones"
    t.string   "hypervisor"
    t.string   "versionHypervisor"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "maquina_id"
  end

  create_table "maquinas", force: :cascade do |t|
    t.string   "nombre"
    t.string   "SO"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "usuario_id"
  end

  create_table "ordens", force: :cascade do |t|
    t.string   "comando"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "maquina_id"
  end

  create_table "snapshots", force: :cascade do |t|
    t.string   "nombre"
    t.string   "direccionArchivo"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "maquina_virtual_id"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.string   "email"
    t.integer  "diferenciaHoraria"
    t.string   "auth"
    t.integer  "resetPassword"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
