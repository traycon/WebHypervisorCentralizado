class MaquinaVirtualsController < ApplicationController
  before_action :set_maquina_virtual, only: [:show,:actualizarMV,:vnc,:accion,:changevnc]
  
  before_action :require_login
 skip_before_action :require_login, only: [:mv]
 
   def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  
    def actualizarMV
      current_user
    ordens=Orden.new
    ordens.comando="hostname"
    ordens.maquina_id=@maquina_virtual.maquina_id
    ordens.save
    redirect_to maquina_path(@maquina_virtual.maquina_id)
    end

  # GET /maquina_virtuals/1
  # GET /maquina_virtuals/1.json
  def show
     current_user
  end
  
  def accion
    current_user
    mv=MaquinaVirtual.all.where(:maquina_id=>Maquina.all.select(:id).where(:usuario_id=> @current_user["id"])).where(:id=>@maquina_virtual.id)
    if mv!=nil
      comando=Comando.find_by(nombre: params[:comando],version: @maquina_virtual.SO,hypervisor: @maquina_virtual.hypervisor)
      if comando!=nil
      cadena=""
      comando.comandoParam.split("##").each {|param|
        if param.eql? "ruta" or param.eql? "rutaHDD" or param.eql? "sizeTotalMV"
          cadena+=@maquina_virtual.opciones.split(param+"=")[1].split(";")[0]
        elsif param.eql? "valor"
          cadena+=params[:valor]
        elsif param.eql? "nombre"
          cadena+=@maquina_virtual.nombre
        else
          cadena+=param
        end
        
      }
      orden=Orden.new
      orden.comando=cadena
      orden.maquina_id=@maquina_virtual.maquina_id
      orden.save
      end
    end
    redirect_to @maquina_virtual
  end
  
  def vnc
    current_user
  end
  
  def changevnc
    current_user
    cadena=@maquina_virtual.opciones.split("ipvnc=")[0]+"ipvnc="+params[:ipvnc].gsub("_",".")+";portvnc="+params[:portvnc]+";"
    @maquina_virtual.opciones=cadena
    @maquina_virtual.save
    redirect_to vnc_path @maquina_virtual
  end
  
  def mv
    @correcto=""
    user = Usuario.find_by(auth: format(params[:key]).to_s)
   maquina=nil
   maquinasvirtuales=nil
   if user!=nil
     maquina=Maquina.find_by(SO: format(params[:so]).to_s,nombre: format(params[:host]).to_s,usuario_id: user.id)
     if maquina!=nil
     mavir=MaquinaVirtual.find_by(hypervisor: format(params[:hypervisor]).to_s,nombre: format(params[:nombre]).to_s,maquina_id: maquina.id)
      if mavir!=nil
        mavir.estado=format(params[:estado]).to_s
        cadena="ruta="+format(params[:ruta]).to_s+";"
        cadena+="ramTotal="+format(params[:memoriaTotal]).to_s+";"
        cadena+="ram="+format(params[:memoria]).to_s+";"
        cadena+="rutaHDD="+format(params[:rutaHDD]).to_s+";"
        cadena+="sizeTotalMV="+format(params[:sizeTotalMV]).to_s+";"
        cadena+="sizeOcupadoMV="+format(params[:sizeOcupadoMV]).to_s+";"
        cadena+="sizeTotalMaquina="+format(params[:sizeTotalMaquina]).to_s+";"
        if mavir.opciones.split("ipvnc=")[1].split(";")[0] != nil
        cadena+="ipvnc="+mavir.opciones.split("ipvnc=")[1].split(";")[0]+";"
        else
        cadena+="ipvnc=;"
        end
        if mavir.opciones.split("portvnc=")[1].split(";")[0] != nil
        cadena+="portvnc="+mavir.opciones.split("portvnc=")[1].split(";")[0]+";"
        else
        cadena+="portvnc=;"
        end
        mavir.opciones=cadena
        mavir.save
        
        mavir.snapshots.destroy_all
        snapshots=format(params[:snapshots]).to_s
          if snapshots!="null"
            snapshots.split(";").each{|snap|
              snapshot=Snapshot.new
              snapshot.nombre=snap
              snapshot.direccionArchivo=snap
              snapshot.maquina_virtual_id=mavir.id
              snapshot.save
            }
          end
        @correcto="todo correcto update maquina"
       else
         newmv=MaquinaVirtual.new
         newmv.nombre=format(params[:nombre]).to_s
         newmv.estado=format(params[:estado]).to_s
         newmv.SO=format(params[:so]).to_s
         newmv.hypervisor=format(params[:hypervisor]).to_s
          cadena="ruta="+format(params[:ruta]).to_s+";"
        cadena+="ramTotal="+format(params[:memoriaTotal]).to_s+";"
        cadena+="ram="+format(params[:memoria]).to_s+";"
        cadena+="rutaHDD="+format(params[:rutaHDD]).to_s+";"
        cadena+="sizeTotalMV="+format(params[:sizeTotalMV]).to_s+";"
        cadena+="sizeOcupadoMV="+format(params[:sizeOcupadoMV]).to_s+";"
        cadena+="sizeTotalMaquina="+format(params[:sizeTotalMaquina]).to_s+";"
        cadena+="ipvnc=;"
        cadena+="portvnc=;"
        newmv.opciones=cadena
        newmv.maquina_id=maquina.id
        newmv.save
       
        snapshots=format(params[:snapshots]).to_s
          if snapshots!="null"
            snapshots.split(";").each{|snap|
              snapshot=Snapshot.new
              snapshot.nombre=snap
              snapshot.direccionArchivo=snap
              snapshot.maquina_virtual_id=newmv.id
              snapshot.save
            }
          end
       @correcto="todo correcto new maquina"
      end
     end
   end
    redirect_to root_path
  end

  # DELETE /maquina_virtuals/1
  # DELETE /maquina_virtuals/1.json
  def destroy
     user = Usuario.find_by(auth: params[:key])
    if user!=nil
     maquina=user.maquinas.find_by(SO: params[:so],nombre: params[:host])
      if maquina!=nil
        maquinavirtual=maquina.maquina_virtuals.find(params[:id])
       begin
        maquinavirtual.destroy
        rescue
          maquinavirtual.delete
        end
      end
    end
    redirect_to root_path
  end
  

  
  private
  def format(e)
  return URI.unescape(e.to_s).to_s.gsub("%3A",":").gsub("_{}_",".").gsub("%3B",";").gsub("%2F","/")
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_maquina_virtual
      @maquina_virtual = MaquinaVirtual.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maquina_virtual_params
      params.require(:maquina_virtual).permit(:nombre, :estado, :SO, :opciones, :hypervisor, :versionHypervisor)
    end
end
