class CreateComandos < ActiveRecord::Migration
  def change
    create_table :comandos do |t|
      t.string :nombre
      t.string :comandoParam
      t.string :hypervisor
      t.string :version

      t.timestamps null: false
    end
  end
end
