require 'test_helper'
include SessionsHelper
class MaquinaVirtualsControllerTest < ActionController::TestCase
  setup do
    @maquina_virtual = maquina_virtuals(:one)
      @user = Usuario.new
       @user.email=  "prueba@prueba.es"
       @user.password= "123456aa"
       @user.username= "prueba"
       @user.auth="1234"
       @user.save
       log_in @user
       @maquina=Maquina.new
    @maquina.nombre="prueba-pc"
    @maquina.SO="Windows"
    @maquina.usuario_id=@user.id
    @maquina.save
    @maquina_virtual.maquina_id=@maquina.id
    @maquina_virtual.hypervisor="VirtualBox"
    @maquina_virtual.SO="Windows"
    @maquina_virtual.save
  end
  
  def teardown
    @user.destroy
    log_out
  end

  test "should show maquina_virtual" do
    get :show, id: @maquina_virtual
    assert_response :success
    
    log_out
    
    get :show, id: @maquina_virtual
    assert_redirected_to root_path
  end

  test "Actualizar MV" do
    get :actualizarMV, id: @maquina_virtual
    assert_redirected_to maquina_path(@maquina.id)
  end

  test "Accion" do
    get :accion, comando: "apagar", id: @maquina_virtual, valor:0
    assert_redirected_to @maquina_virtual
  end

  test "vnc" do
    get :vnc,id: @maquina_virtual
    assert_response :success
  end
  
    test "changevnc" do
    get :changevnc,id: @maquina_virtual, ipvnc:127_0_0_1, portvnc:5900
    assert_redirected_to vnc_path(@maquina_virtual.id)
  end
  
    test "destroy" do
        assert_difference('MaquinaVirtual.count', -1) do
          get :destroy, key: @user.auth, so: @maquina.SO ,host: @maquina.nombre, id: @maquina_virtual
        end

        assert_redirected_to root_path
  end
  
      test "mv" do
        get :mv,key: @user.auth, so: @maquina.SO ,host: @maquina.nombre, hypervisor:"asd",nombre:"asd",ruta:"asd",memoriaTotal:8000,memoria:2000,estado:"running",rutaHDD:"asd",sizeTotalMV:4000,sizeOcupadoMV:2000,sizeTotalMaquina:3500,snapshots:"aasd;"
        assert_redirected_to root_path
        
        get :mv,key: @user.auth, so: @maquina.SO ,host: @maquina.nombre, hypervisor:"asd",nombre:"asd",ruta:"asd1",memoriaTotal:8000,memoria:2000,estado:"running",rutaHDD:"asd",sizeTotalMV:4000,sizeOcupadoMV:2000,sizeTotalMaquina:3500,snapshots:"aasd;"
        assert_redirected_to root_path
  end
  
end
