class AddMaquinaVirtualIdToSnapshots < ActiveRecord::Migration
  def change
    add_column :snapshots, :maquina_virtual_id, :integer
  end
end
