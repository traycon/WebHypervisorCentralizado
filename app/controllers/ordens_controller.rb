class OrdensController < ApplicationController
  before_action :set_orden, only: [:destroy]
  layout false
 before_action :require_login
 skip_before_action :require_login, only: [:ordenes, :existe_user,:existeMaquina,:delete]
 
   def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end

  def ordenes
    user = Usuario.find_by(auth: params[:key])
   maquina=nil
   @ordens=nil
   if user!=nil
     maquina=user.maquinas.find_by(SO: params[:so],nombre: params[:host])
     if maquina!=nil
     @ordens=user.maquinas.find_by(SO: params[:so],nombre: params[:host]).ordens
   end
   end
  end
  def existe_user
     @user = Usuario.find_by(auth: params[:id])
  end
  
   def existeMaquina
   @user = Usuario.find_by(auth: params[:key])
   @maquina=nil
   if @user!=nil
     @maquina=@user.maquinas.find_by(SO: params[:so],nombre: params[:host])
   end
   
 end

  # DELETE /ordens/1
  # DELETE /ordens/1.json
  def destroy
    @orden.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: '' }
      format.json { head :no_content }
    end
  end
  
 def delete
    user = Usuario.find_by(auth: params[:key])
   maquina=nil
   @orden=nil
   if user!=nil
     maquina=user.maquinas.find_by(SO: params[:so],nombre: params[:host])
     if maquina!=nil
     @orden=user.maquinas.find_by(SO: params[:so],nombre: params[:host]).ordens.find(params[:id])
      if @orden!=nil
      @orden.destroy
      end
     end
   end
   redirect_to root_path
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orden
      @orden = Orden.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orden_params
      params.require(:orden).permit(:comando)
    end
end
