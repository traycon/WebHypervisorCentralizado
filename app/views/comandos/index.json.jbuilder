json.array!(@comandos) do |comando|
  json.extract! comando, :id, :nombre, :comandoParam, :hypervisor, :version
  json.url comando_url(comando, format: :json)
end
